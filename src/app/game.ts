export type Player = 'X' | 'O';

export type CellIndex = {
  row: number;
  col: number;
};

export interface Cell extends CellIndex {
  player?: Player
}

export type Grid = Row[]
export type Row = Col[]
export type Col = Cell

export interface Game {
  currentPlayer: Player

  // cols -> cell -> Player
  grid: Grid
}
