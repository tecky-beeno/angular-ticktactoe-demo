import {Injectable} from '@angular/core';
import {CellIndex, Game, Player} from "./game";
import {Observable, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private url_prefix = 'http://localhost:3000/game'
  private currentGame?: Game
  highlightPlayer?: Player

  private gameSubject = new Subject<Game>()

  constructor() {
    setInterval(() => this.refresh(), 500)
  }

  refresh() {
    fetch(this.url_prefix)
      .then(res => res.json())
      .then(game => {
        this.gameSubject.next(game)
      })
  }

  subscribeGame(): Observable<Game> {
    if (this.currentGame) {
      setTimeout(() => {
        this.gameSubject.next(this.currentGame)
      })
    } else {
      this.refresh()
    }
    return this.gameSubject.asObservable()
  }


  place(index: CellIndex) {
    fetch(this.url_prefix + '/place', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({index})
    }).then(res => res.json())
      .then(game => this.gameSubject.next(game))
  }

  reset() {
    fetch(this.url_prefix + '/reset', {
      method: 'POST',
    }).then(res => res.json())
      .then(game => this.gameSubject.next(game))
  }
}
