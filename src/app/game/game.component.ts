import {Component} from '@angular/core';
import {GameService} from "../game.service";
import {CellIndex, Player} from "../game";


@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent {
  now = new Date()
  game = this.gameService.subscribeGame()

  constructor(public gameService: GameService) {
  }

  refresh(){
    this.gameService.refresh()
  }


  reset() {
    this.gameService.reset()
  }


  set highlightPlayer(player: Player | undefined) {
    this.gameService.highlightPlayer = player
  }

  get highlightPlayer() {
    return this.gameService.highlightPlayer
  }

  place(index: CellIndex) {
    this.gameService.place(index)
  }
}
